# pub-kea-sd18i-db-orm

# Object Relational Mapper

In this lesson, you will learn to use an object relational mapper for communication between an object oriented language (Python) and an SQL based database.

## Install Python

First, you need to make sure Python 3.7 or greater is installed on your system. Please note, that you might have several versions installed on your system.

```bash
$ python -V
Python 3.7.0
```

If your system has version 2.x installed, you might also have version 3.x installed. 
Try `python3 -V` to see if this is the case.
If you get something like `Python 3.6.5 :: Anaconda, Inc.`you will still need to install Python 3.7 or greater.

Make sure you are using the right version of python.
To use the correct version on my system, I have to do:

```bash
$ /usr/local/bin/python3 -V
```

Use your package manager (Linux and Unix), Homebrew (on macOS), or download the installer (Windows).
You can find lots of help for your OS on the Internet.

## Setting up a virtual environment

When working with Python, it is recommended to create a virtual environment to avoid conflicts between different prokects.
Using the correct version of Python as shown above, create a virtual environment:

```bash
$ python3 -m venv sd18i-db
```

This command will create a foler `sd18i-db` with your virtual environment.
Make sure you put this in a suitable location (e.g. your home folder).

Now, you can activate the virtual environment:

```bash
$ source sd18i-db/bin/activate
(sd18i-db) henrik@hstr:py-env
$
```

If everything went well, your terminal prompt should now be prefixed with `(sd18i-db)`.

To get out of the virtual environment, use the command `deactivate`, but for now you should not do that.

If you close down your computer and want to continue working on this lesson, you need to run the `source` command again.

## Cloning this repository

To clone the starter project for this lesson, run this command:

```bash
$ git clone https://henrikstroem@bitbucket.org/henrikstroem/pub-kea-sd18i-db-orm.git
```

This will create a folder named `pub-kea-sd18i-db-orm` in your current directory.

## Install Python packages

To install the Python packages needed for this lesson, run this command:

```bash
pip -r requirements.txt
```

You now have an environment ready for Django development.

# Tutorial

To familiarize yourself with Django, you can go through the Django tutorial at [Getting Started](https://docs.djangoproject.com/en/2.1/intro/).
It's quite long, but you can focus on the things that have to do with *models*.

# Detailed information about the Django Model layer

The Django object relational mapper is implemented in the Model layer.
You can find detailed information abould it here: [The Model Layer](https://docs.djangoproject.com/en/2.1/#the-model-layer)

The page also contains information on other aspects of Django.

